(defpackage #:story-test
  (:use #:cl #:story-engine)
  (:export #:test))

(in-package #:story-test)

(defun callables (&rest names)
  (dolist (name names)
    (format t "~a: ~a, " name (if (event-callable-p (get-event name)) "yes" "no")))
  (format t "~%"))

(defun test ()
  (with-new-story
    (def "intro" (par))
    (expressions
     "path: intro"
     "alt: intro")
    (callables "intro" "path" "alt") ;only intro is callable
    (call-event (get-event "intro")) 
    (callables "intro" "path" "alt") ;intro still callable, also path and alt
    (call-event (get-event "path"))
    (callables "path" "alt") ;path and alt are both not callable anymore
    ;;(expression "next-path: path")
    (callables "next-path") ;next-path should already be callable, even if just added
    (expression "other-alt: intro")
    (callables "other-alt") ;other-alt should not be callable, since it depends on used intro
    (expressions "reunite: path" "reunite: alt")
    (callables "reunite") ;should be callalbe
    ;;(expression "next-alt: alt")
    (call-event (get-event "intro"))
    (call-event (get-event "alt"))
    (call-event (get-event "reunite")) ;not yet possible to call
    (callables "next-path" "next-alt") ;both should be callable, but alternative, even if they have no same event required
    ))
