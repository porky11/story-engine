### What is this?

See documentation in file `doc.md`
This is not finished yet.

### Installation

Clone this repo into `path/to/quicklisp/local-projects/`, start some common lisp with quicklisp installed and call `(ql:quickload :story-engine)`
Test the example using `(story-engine:test)`

### Test

See the current test program in file `engine.lisp`

