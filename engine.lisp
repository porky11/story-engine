(defpackage #:story-engine
  (:use #:cl #:alexandria #:split-sequence)
  (:export #:with-new-story
           #:par
           #:get-event
           #:expression
           #:expressions
           #:event-callable-p
           #:call-event
           #:def))

(in-package #:story-engine)


;;;structs

(defstruct path
  ;;the event, it belongs to
  (event (error "No event supplied"))
  ;;storiesq, that have to be completed, to call this path
  (before (error "No stories before this supplied"))
  ;;how often this path was called
  (count 0)
  ;;how often are required stories called
  (called (make-hash-table))
  ;;check, if this path can be called
  callable-p)

(defstruct event
  ;;name of the event
  (name (error "No name supplied"))
  ;;paths, that depend on this event
  after
  ;;paths, this event can be called with
  paths
  ;;list of delayed paths
  ;;everything, that depends on an event has to look here, to find, if it still matches
  delayed
  ;;check, if this event can be called
  callable-p)


;;;global state

(defvar *story* (make-hash-table :test #'equal))

(defmacro with-new-story (&body body)
  `(let ((*story* (make-hash-table :test #'equal)))
     ,@body
     *story*))


;;;running

(defun compute-path-callable-p (path)
  (setf (path-callable-p path) t)
  (loop for before-num being the hash-value of (path-before path)
     using (hash-key before-event)
     do (when (< (gethash before-event (path-called path)) before-num)
          ;;(format t "event not callable~%")
          (return-from compute-path-callable-p (setf (path-callable-p path) nil))))
  t)

(defun compute-event-callable-p (event)
  (setf (event-callable-p event)
        (remove-if-not #'path-callable-p (event-paths event))))


(defun path-parallel-p (after allow event)
  (declare (ignorable after))
  (format t "try ~S ~S~%"
          (event-name (path-event allow)) 
          (event-name (path-event event)))
  (eq allow event))
  

;;;construction

(defun get-event (name)
  (ensure-gethash name *story* (make-event :name name)))

(defun add-path (name before-names)
  (let* ((event (get-event name))
         (before-list (mapcar #'get-event before-names))
         (before (let ((table (make-hash-table)))
                   (dolist (name before-names)
                     (incf (gethash (get-event name) table 0)))
                   table))
         (path (make-path :before before :event event)))
    ;;when adding new event while running, add called objects
    (dolist (bef before-list)
      (dolist (bef-path (event-paths bef))
        (incf (gethash bef (path-called path) 0) (path-count bef-path)))
      ;;Delete all called alternative stories:
      ;;for all paths that require before stories,
      (dolist (dep-path (event-after bef))
        ;;TODO: Some conditions, that allow parallel followers
        ;;decrease the hash key of this specific event in the new path by the number of calls of the other path
        (decf (gethash bef (path-called path)) (path-count dep-path))))

    (compute-path-callable-p path) ;compute, if path is already callable

    ;;TODO: replace more specific paths, use hash tables
    (if (or (not (event-paths event))
               ;;if there already are paths to call this event
               ;;for all paths to this event
               (dolist (path (cons path (event-paths event)) )
                 ;;if one of the paths before this is not parallel to this
                 (loop for bef being the hash-key of (path-before path)
                    do (when
                           (or (dolist (after (event-after bef) t) ;default here: all event paths are parallel
                                 (unless (path-parallel-p bef after path)
                                   (return)))
                        ;;or if the paths may be parallel in some case
                               nil)
                         (return t)))))
        (push path (event-paths event)) ;add new paths to event
        (error "Adding path to ~S not possible: Some paths may conflict now" (event-name event)))
    ;;all before stories will have to check all dependant paths
    (dolist (name before-names)
      (pushnew path (event-after (get-event name))))
    ;;compute, if event is already callable
    (if (path-callable-p path)
        (push path
              (event-callable-p event)))
    #+nil
    (compute-event-callable-p event)))


;;;s-expression language
 
(defmacro par (&body body)
  `(list ,@body))

(defmacro def (name-decl par-decl)
  (let ((name
         (if (atom name-decl)
             name-decl
             (case (car name-decl)
               (t (error "Name declaration ~a undefined" (car name-decl))))))
        (par
          (if (atom par-decl)
              `(par ,par-decl)
              (case (car par-decl)
                ((par) par-decl)))))
    `(add-path ,name ,par)))



;;;language parsing

(defvar *whitespaces*
  '(#\Space #\Newline #\Backspace #\Tab 
    #\Linefeed #\Page #\Return #\Rubout))

(defun split-string (string split)
  (do
   ((i 0 (1+ i)))
   ((char\= (char string i) split)
    (values (subseq string 0 i) (subseq string (1+ i))))))



(defun parse-expression (string)
  (let ((char
         (loop for char in (cons #\newline (coerce ":;" 'list))
            do (let ((char (find char string)))
                 (when char
                   (return char))))))
    (case char
      ;;by default trim spaces and check for valid names
      ((nil) (let ((string (string-trim *whitespaces* string)))
               (if (every (lambda (char) (or (char= char #\-) (alpha-char-p char))) string)
                   (if (string= string "")
                       '(empty)
                       string)
                   (error "Names can only contain letters and `-`"))))
      ;;multiple expressions in multiple lines
      (#\newline `(progn ,@(mapcar #'parse-expression (split-sequence char string))))
      ;;define event
      (#\: (multiple-value-bind (first second)
               (split-string string char)
             `(def ,(parse-expression first) ,(parse-expression second))))
      ;;remove comments
      (#\; (parse-expression (split-string string char))))))

(defmacro expression (string)
  (parse-expression string))

(defmacro expressions (&body strings)
  `(progn ,@(mapcar #'parse-expression strings)))



;;;running



(defun call-path (path)
  ;;(format t "Calling a path of event ~S~%" (event-name (path-event path)))
  (incf (path-count path))
  (loop for before-num being the hash-value of (path-before path)
     using (hash-key before-event)
     do (dolist (delpath (event-after before-event))
          ;;(format t "Decrease ~S of path ~S~%" (event-name before-event) (event-name (path-event delpath)))
          (decf (gethash before-event (path-called delpath)))
          (unless (compute-path-callable-p delpath)
            (compute-event-callable-p (path-event delpath))))))



(defun call-event (event)
  (if-let ((paths (event-callable-p event)))
    (progn
      ;;either have only a single path
      (if (cdr paths)
          ;;or all paths have to be parallel to everything
          (let ((conflicts))
            (declare (ignore conflicts))
            (if (every (lambda (path)
                         (block parallel
                           (loop for event being the hash-key of (path-before path)
                              do (dolist (after (event-after event))
                                   (unless (path-parallel-p event after path)
                                     (return-from parallel))))
                           t))
                       paths)
                (call-path (car paths))
                (error "There are mulitple possible paths, that may affect callability")))
          (call-path (car paths)))
      ;;recompute if event is still callable
      (compute-event-callable-p event)
      ;;for all paths, that depend on this event:
      (dolist (path (event-after event))
        ;;increase the count of this event
        (incf (gethash event (path-called path)))
        ;;recompute if path is now callable
        ;(format t "we are here ~%")
        (when (compute-path-callable-p path)
          ;;if now callable, add this path to the callable stories
          ;(format t "Event ~S should now be callable~%" (event-name (path-event path)))
          (push path (event-callable-p (path-event path))))))
    (error "Event ~S not callable" (event-name event))))


