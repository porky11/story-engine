# Requirements
* Define stories with linear, alternative, parallel, optional and cyclic story lines out of events
* Adding new events without editing old entries or breaking working stories
* Simulate these stories interactively
* Adding new stories during the simulation

# Story lines

### Linear

An event can be called after another event is called.

### Alternative

Multiple events can be called alternatively after one of multiple events is called.

### Parallel

Multiple events can be called parallel after multiple events are called.

### Optional

A event may only be called, before another event is called

### Cyclic

Stories can be called multiple times.


# Simulation

It is possible to find out, which events can be called.

After calling an event, other events may be possible.

The user should be able to select one of the possible events, for example by selecting from a list of possible events.

Selecting an event could also trigger something, like displaying a story.




