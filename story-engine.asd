(asdf:defsystem #:story-engine
  :author "Fabio Krapohl <fabio.u.krapohl@fau.de>"
  :description "Engine for writing stories with parallel and alternate story lines"
  :license "LLGPL"
  :depends-on (#:alexandria #:split-sequence)
  :components ((:file "engine")
               (:file "test")))


