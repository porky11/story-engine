Story-Language
==============

## Goals:

* A language for creating complex stories composed out of linear stories.
* Support for parallel and alternative story lines.
* The posibility of adding new stories and constraints without having to modify or breaking the currently finished story.
* Disallowing unstructured and impossible stories.
* The ability to run the story as program add new stories to a story, also during execution (this allows interactive editable stories)


## Description:

It's not yet implemented, and I'm not sure, if it will work, as I describe.
If you have ideas, how to make it better, you may submit issues.
If it works, I will probably implement it more efficient in another programming language. Here I just try to get it working somehow.
Currently only alternative stories work. See function `#'test` in engine.lisp.


## The Language:

In each line one expression is written.
A single story is defined by a symbol:
```
intro
```
To set it callable by default, write this:
```
intro: *
```

This way you define a story, that comes after another story:
```
chapter: intro
```
This means `chapter` comes after `intro`.

You also can add multple chapters at once using this:
```
second-chapter: first-chapter: intro
```

If you now want to define an alternative to `chapter` this can be done this way:
```
alternative: intro
```
This means `alternative` also follows `intro`.

You can define multiple alternative stories using this syntax:
```
chapter|alternative: intro
```
This will expand to the last two expressions.

You may want, that multiple stories result in the same story. Therefore you just have to define a story multiple times:
```
next: chapter
next: alternative
```
You can also write this instead:
```
next: chapter|alternative
```

That's all you need to know about alternative story lines for now.
If you want multiple story lines to be parallel, you can write this:
```
chapter|parallel: intro
intro/chapter&parallel
```
The second line means, that `chapter` and `parallel` come after `intro`.
So parallel stories and alternative stories are defined the same way, but when some stories should be parallel, it has to be added explicitely.
The reason for this is, that if you have a working story, and add some parallels, it may not work as before anymore.
The reason, that you have to write `intro/` is, that there may be another story like this:
```
chapter|parallel: another-story
```
In this case, they would be alternative, when `another-story` comes before.
You cannot write something like this instead:
```
chapter&parallel: intro
```
(This may be confusing, because of next syntax. Is this really useful?)

When there are multiple story lines, it also has to be possible to unify two story lines like this:
```
next: chapter&parallel
```

By default, it's only possible to call a story once. If you want a story to be callable multiple times, you have to write this:
```
story!
```
The name of the story is just `story`
When you want the story to be called multiple times at creation time, you can write this:
```
story!: intro
```

You can also add comments:
```
chapter: intro ;chapter comes after intro
;;In this chapter, the main person will start to do something
```

For simplification it is also possible to add variables like this:
```
var = chapter|alternative&parallel
```
When writing `var` somewhere, it will just be replaced before execution.
(Should variables only allow `|`, because they work may work like c macros, and lead to wrong operator precedence or should they be expanded in a more complex way?)

What, if you made a big chapter, and want to be able to have alternatives inside it?
```
intro
first-chapter: intro
second-chapter: first-chapter

;;during the first chapter, you want to add stories
first-chapter = second-scene: first-scene
;;first-chapter will be treated as a variable, that expands to second-scene
;;the both statments above will expand to this:

second-scene: first-scene: intro ;;first scene comes after intro, second scene after first scene 
second-chapter: second-scene: first-scene ;;second chapter comes after second scene, second scene still after first scene

;;now you can add something like this:
alternate-second-scene: first-scene
second-chapter: alternate-second-scene ;;and still after second scene

;;when i had something like this:
first-chapter\second-chapter&alternate-chapter
;;it will expand to this:
first-scene: second-scene/second-chapter&alternate-chapter

```

## Checking validity:

To ensure, that all stories can be compiled unambiguously, following condition is required: (still wrong conditions)
When a story comes after multiple alternatives, these alternatives are required never to be parallel.
When a story comes after multiple parallels, it has to be possible for them, to be parallel.

This will be checked everytime a new story is added, some stories are made parallel, or a story is made to be callable not once only.
To see, why this may be needed, see this example:
```
a: x|y
b: y
```
Now assume, `x` and `y` can be parallel and have been just called.
When you call `a` first, can you still call `b`? Because, when you call `y` first and then `b` first, `a` cannot be called anymore, after calling `b`.
When just choosing the first or a random possibility, this is not deterministic anymore. It would also be possible to allow both, until one is selected.
The following two would also be valid, even if a and b are parallel:
```
a: x|y
```
```
a: x
b: y
```
If possible, it's preferable, to implement a workaround, so both can be called in any order. It may just be difficult to implement.



# Something

To check, if two stories are parallel, they need to have at least one way to be called parallel.
A story will check for all of their alternate lists of parallel prequels, if it can 


## Representation:

Most cases could be represented as petri nets, but some cannot:
```
;;Example
a: x|y
b: y
```
`a` could be represented as a transition, that depends on a place being filled by `x` and `y`
`b` could be represented as a transition, that depends on a place being filled by `y`
But `a` and `b` cannot be alternative that way, since the transitions are filled by different transitions.
Some other things may be possible, but complicated to represent.

The new representation is simpler:
A story owns a place, where tokens are in, which identify the story, that generated them.
It also defines a list of alternate sets of tokens, that it has to contain, to be called.
For each story, a set of tuples is defined, which represent, which stories are parallel after another story.
When a story is called, and there is only one set of tokens, that contains all tokens of the storie's place, they get deleted in this place, and every alternate place, which is a place of a story, not defined as parallel to this story, will be deleted.
If there are multiple alternate sets, just the tokens, that are contained by all sets, are deleted. The alternative tokens are deleted, when the alternatives aren't possible anymore.
TODO: This is a bit more complicated.
After a story is called, it will fill all places (at least all relevant places) with a token, that identifies the story.



## Lisp Implementation:
```
chapter: intro
;;will expand to
(add-story "chapter" (par "intro"))

a|b
;;will expand to
(alt "a" "b")

a|b|c
;;will expand to
(alt "a" "b" "c")

a&b
;;will expand to
(par a b)

intro\a&b
;;will expand to
(make-parallel "intro" (par "a" "b"))

intro!
;;will expand to
(multiple "intro")
```

Expressions containing `alt` will expand to multiple expressions like this:
```
(add-story (alt "chapter" "alternative") (alt "intro" "another-story"))
;;will expand to
(progn
  (add-story "chapter" "intro")
  (add-story "alternative" "intro")
  (add-story "chapter" "another-story")
  (add-story "alternative" "another-story"))
```

`add-story` will add a story
`make-parallel` will make multiple stories parallel
`par` will be implemented as `#'list`


## Example story:

```
intro: * ;can be called at beginning
;;You live at home.

meet-friend: intro
;;You go to your friend and you have fun.

go-away: intro|meet-friend ;this should not be modelled as parallel, since you don't want to be able to meet your friend after going away
;;You go to the city, because you want to live there.

funny-journey: go-away&meet-friend ;parallel
;;Your friend follows you, because you had fun before.
meet-friend/go-away&intro ;this is required, since else this would never be callable, because `meet-friend` is consumed by `go-away` 

boring-journey: go-away&intro
;;You are alone and the journey gets boring
intro/go-away&meet-friend ;this way you say, that `meet-friend` was not called.

reach-city: funny-journey|boring-journey
;;You reach the city.

inside-city!: reach-city ;this can be called multiple times
;;You are inside the city and look around. Where do you want to go now?

go-building|go-train: inside-city ;define multiple stories at once
;;You go into a building, but you leave it again
;;You go into a train, but you leave it again

inside-city: go-bouilding|go-train ;this will not redefine `inside-city`, but add some alternative ways to call it. The `!` is not needed, since it was defined before.

```

Other examples:
```
find-key-1: …
find-key-2: …
find-key-3: …

found-key!: find-key-1|find-key-2|find-key-3 ;this extra story is not required, you may prefer not to use an extra story
;;You found a Key

open-door: found-key&found-key&found-key ;it's possible to depend on the same story multiple times
```

## Problems:

How to model this: Everytime you go to your friend, he gives you up to one steak and one bonbon (you can visit him multiple times):
```
visit-friend!: *
bonbon!: visit-friend
steak!: visit-friend
leave-friend!: visit-friend
visit-friend!: leave-friend ;you can visit him again

;;approach 1: parallel
visit-friend\steak&bonbon ;now you cannot leave anymore, after you took something
visit-friend\steak|bonbon&leave-friend ;now you can take a bonbon and steak after leaving

;;approach 2: alternatives
leave-friend: bonbon|steak ;now you can only take one steak or one bonbon
bonbon: steak ;now you have to take the steak first, to get the bonbon too
steak: bonbon ;now you can take as much bonbons and steaks, as you want, if you take them alternating

;;approach 3: start with parallel
visit-friend\steak&bonbon
leave-friend: steak|bonbon ;steak and bonbon are parallel. You will be able to leave the friend twice

;;approach 4: similar
visit-friend\steak&bonbon
leave-friend: steak&bonbon ;you can either take both, a bonbon and a stake, or nothing

;;approach 5: restart and add additional stories (both, especially restart, should not be required in this language)
has-bonbon|has-steak!: *
visit-friend: has-steak&has-bonbon
bonbon!: visit-friend&has-bonbon
steak!: visit-friend&has-steak
leave-friend!: visit-friend
visit-friend\bonbon&steak&leave-friend

has-steak!: steak&leave-friend ;refill steak, if you took one
has-bonbon!: bonbon&leave-friend ;refill bonbon, if you took one

has-bonbon\visit-friend&bonbon
has-steak\visit-friend&steak

;;why does this approach not work without refactoring?
;;what has to be changed, to let it work?

```

